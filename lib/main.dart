import 'package:flutter/material.dart';
import 'package:flutteruser/views/home.dart';
import 'package:get/get.dart';

void main() {
  runApp(GetMaterialApp(
    theme: ThemeData(
        iconTheme: IconThemeData(color: Colors.white),
        primaryColor: Colors.redAccent,
        backgroundColor: Colors.white,
        buttonColor: Colors.blue,
        brightness: Brightness.light,
        buttonTheme: ButtonThemeData(
          buttonColor: Colors.redAccent,
          padding: EdgeInsets.all(10.0)
        )
    ),
    home: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Home();
  }
}

