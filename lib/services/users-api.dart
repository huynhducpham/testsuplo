import 'dart:convert';
import 'package:flutteruser/models/users.dart';
import 'package:http/http.dart' as http;

class UserApi{
  final baseUrl = "http://5ee4f19cddcea00016a37090.mockapi.io";
  Future<List<Users>> getUser() async {
    final response = await http.get("$baseUrl/api/v1/users", headers: {"Content-Type": "application/json"});
    if(response.statusCode == 200){
      final data = jsonDecode(response.body).cast<Map<String, dynamic>>();
      return data.map<Users>((json) => Users.fromJson(json)).toList();
    } else return [];
  }
  Future<bool> deleteUser(String id) async {
    final response = await http.delete("$baseUrl/api/v1/users/$id");
    if(response.statusCode == 200){
      return true;
    } else return false;
  }

  Future<bool> postUser(String email, String fullName) async {
    final body = jsonEncode({
      "email": email,
      "name": fullName
    });
    final response = await http.post("$baseUrl/api/v1/users",
        body: body,
        headers: {"Content-Type": "application/json"});
    if(response.statusCode == 201){
      return true;
    }else
    return false;
  }
  Future<bool> updateUser(String email, String fullName, String id) async {
    final body = jsonEncode({
      "email": email,
      "name": fullName
    });
    final response = await http.put("$baseUrl/api/v1/users/$id",
        body: body,
        headers: {"Content-Type": "application/json"});
    if(response.statusCode == 200){
      return true;
    }else
    return false;
  }



}