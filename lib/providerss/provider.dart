import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutteruser/models/users.dart';
import 'package:flutteruser/services/users-api.dart';
import 'package:get/get.dart';

class HomeProvider extends ChangeNotifier{
  StreamController<List<Users>> userStreamCtrl = StreamController();
  Stream get userStream => userStreamCtrl.stream;

  UserApi userApi = new UserApi();
  int page = 1;

  TextEditingController edtEmail = new TextEditingController();
  TextEditingController edtFullName = new TextEditingController();

  loadUser(){
    userApi.getUser().then((value) => userStreamCtrl.sink.add(value));
  }

  HomeProvider(){
    loadUser();
  }

  deleteUser (String id){
    userApi.deleteUser(id).then((value) {
      if(value){
        Get.snackbar("Deleted", "Deleted user: $id");
        loadUser();
      }
      else return Get.snackbar("Deleted faild", "Error with action delete: $id");
    });
  }

  insertUser(){
    if(edtEmail.text.length < 1 || edtFullName.text.length < 1 ){
      return null;
    }else {
      userApi.postUser(edtEmail.text, edtFullName.text).then((value) {
        if(value){
          Get.snackbar("Succesful", "Inserted");
          Get.back();
        }else{
          Get.snackbar("Add new user error", "Try again late");
        }
      });
    }
  }

  updateUser(String id){
    if(edtEmail.text.length < 1 || edtFullName.text.length < 1 ){
      return null;
    }else {
      userApi.updateUser(edtEmail.text, edtFullName.text, id).then((value) {
        if(value){
          Get.snackbar("Succesful", "Updated");
          Get.back();
        }else{
          Get.snackbar("Update user error", "Try again late");
        }
      });
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    userStreamCtrl.close();
  }

}