import 'package:flutter/material.dart';
import 'package:flutteruser/providerss/provider.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class InsertUser extends StatelessWidget {
  String title;
  String id;
  String email;
  String name;
  InsertUser({Key key,this.id, this.title, this.email, this.name}) : super (key: key);
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => HomeProvider(),
      child: insertUser(),
    );
  }

  insertUser(){
    return Consumer<HomeProvider>(
      builder: (_, home, child){
        home.edtEmail.text = email;
        home.edtFullName.text = name;
        return Scaffold(
          appBar: AppBar(
              title: Text("$title"),
              centerTitle: true
          ),
          body: Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                TextFormField(
                  controller: home.edtEmail,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    hintText: "Email",
                    labelText: "Email",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0),
                        borderSide: BorderSide(color: Colors.grey, width: 1),
                        gapPadding: 0.5
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0),
                        borderSide: BorderSide(color: Colors.grey, width: 1),
                        gapPadding: 0.5
                    ),
                    labelStyle: TextStyle(color: Colors.grey),
                  ),
                  keyboardType: TextInputType.emailAddress,
                ),
                SizedBox(height: 12.0),
                TextFormField(
                  controller: home.edtFullName,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                      hintText: "Full name",
                      labelText: "Full name",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0),
                          borderSide: BorderSide(color: Colors.grey, width: 1),
                          gapPadding: 0.5
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0),
                          borderSide: BorderSide(color: Colors.grey, width: 1),
                          gapPadding: 0.5
                      ),
                      labelStyle: TextStyle(color: Colors.grey)
                  ),
                ),
                SizedBox(height: 12.0),
                FlatButton(
                  textColor: Colors.white,
                  padding: EdgeInsets.all(14.0),
                  child: Text("$title"),
                  color: Get.theme.primaryColor,
                  onPressed: () => title == "add" ? home.insertUser() : home.updateUser(id),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
