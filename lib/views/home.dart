import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutteruser/models/users.dart';
import 'package:flutteruser/providerss/provider.dart';
import 'package:flutteruser/views/insert-user.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      child: HomePage(),
      create: (context) => HomeProvider(),
    );
  }
}
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    HomeProvider homeProvider = Provider.of<HomeProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("List user"),
        centerTitle: true,
      ),
      body: Container(
        child: StreamBuilder<List<Users>>(
          stream: homeProvider.userStream,
          builder: (context, snapshot){
            return snapshot.hasData ? itemUser(snapshot.data)
                : Center(child: CircularProgressIndicator());
          },
        ),
      ),
      floatingActionButton: RaisedButton.icon(
        label: Text("Add"),
        textColor: Colors.white,
        onPressed: () => Get.to(InsertUser(title: "add",)).then((value) => homeProvider.loadUser()),
        color: Theme.of(context).primaryColor,
        icon: Icon(Icons.add)),
    );
  }

  Widget itemUser(List<Users> userList) {
    HomeProvider homeProvider = Provider.of<HomeProvider>(context);
    return ListView.builder(
      itemCount: userList.length,
      itemBuilder: (context, index){
        final image = NetworkImage(userList[index].avatar);
        return Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.2,
          secondaryActions: <Widget>[
            IconSlideAction(
              caption: 'Update',
              color: Colors.black45,
              icon: Icons.edit,
              onTap: () => Get.to(InsertUser(
                id: userList[index].id,
                title: "update",
                email: userList[index].email,
                name: userList[index].name)).then((value) => homeProvider.loadUser()),
            ),
            IconSlideAction(
              caption: 'Delete',
              color: Colors.red,
              icon: Icons.delete,
              onTap: () => homeProvider.deleteUser(userList[index].id),
            ),
          ],
          child: ListTile(
            title: Text("${userList[index].name}"),
            subtitle: Text(userList[index].email),
            leading: Container(
              width: 56,
              height: 56,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: image
                )
              ),
            ),
            onTap: () => Get.snackbar("Vuốt để thực hiện", "Vuốt sang trái để hiện tác vụ sửa và xóa"),
          ),
        );
      },
    );
  }

}
